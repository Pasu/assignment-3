$(function() {
    $('#upload-btn').click(function() {
        var form_data = new FormData($('#upload-file')[0]);
        var routing_key = makeUniqueid(10);
        var listUrl = window.location.href.split("?");
        document.getElementById("status").innerHTML = "Sedang mengunggah file ke server 1"
        $.ajax({
            type: 'POST',
            headers: {"X-ROUTING-KEY": routing_key},
            url: listUrl[0],
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function(response) {
                console.log(response.status)
                document.getElementById("status").innerHTML = response.status;
                WebSocket(routing_key)
            },
            error: function(response) {
                console.log(response)
            }
        });
    });
});

function makeUniqueid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }
 function WebSocket(routing_key){
    if ("WebSocket" in window) {
        var ws_stomp_display = new SockJS( 'http://152.118.148.95:15674/stomp');
        var client_display = Stomp.over(ws_stomp_display);
        var mq_queue_display = "/exchange/1606896174/" + routing_key;

        var on_connect_display = function() {
        console.log('connected');
        client_display.subscribe(mq_queue_display, on_message_display);
        };

        var on_error_display = function() {
        console.log('error');
        };

        var on_message_display = function(m) {
        console.log(m.body);
        document.getElementById("progress").style.display = "block";
        document.getElementById("progress").innerHTML = m.body + "% Complete (Success)";
        var persenan = m.body+"";
        if(persenan == "100"){
            document.getElementById("status").innerHTML = "Sukses melakukan kompres"
        }
        };

        client_display.connect('0806444524', '0806444524', on_connect_display, on_error_display, '/0806444524');
        } else {
        // The browser doesn't support WebSocket
        alert("WebSocket NOT supported by your Browser!");
    }
 }