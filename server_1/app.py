from flask import Flask, render_template, request, jsonify, flash
import requests
import random
import string


app = Flask(__name__)



@app.route('/')
def homeindex():
    return render_template('homepage.html')

@app.route('/', methods=['POST'])
def upload():
    if 'file' not in request.files:
        flash('No file part')
        return 'Tidak ada file.'
    url = "http://infralabs.cs.ui.ac.id:20685/"
    files = request.files['file'].read()
    route_key = request.headers['X-ROUTING-KEY']
    headers = {'X-ROUTING-KEY': route_key}
    requests.post(url, files = {'file':files}, headers = headers)
    return jsonify(status="Upload ke server 2")



if __name__ == '__main__':
    app.run(debug=True)